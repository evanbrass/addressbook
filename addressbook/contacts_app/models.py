from django.db import models
from django.forms import ModelForm, ValidationError
import re



class Contact(models.Model):
    first_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=20, blank=True, null=True)
    phone_number = models.CharField(max_length=20, blank=True, null=True)
    email = models.EmailField(blank=True, null=True)
    street = models.CharField(max_length=100, blank=True, null=True)
    city = models.CharField(max_length=50, blank=True, null=True)
    state = models.CharField(max_length=2, blank=True, null=True)
    zip = models.CharField(max_length=5, blank=True, null=True)

    def __unicode__(self):
        return "%s %s" %(self.first_name, self.last_name)


class ContactForm(ModelForm):

    class Meta:
        model = Contact
        fields = ['first_name', 'last_name', 'phone_number', 'email', 'street', 'city', 'state', 'zip']

    def clean_phone_number(self):
        phone_number = self.cleaned_data['phone_number']
        phone_format = re.compile('^1?-?(\d{3})-?(\d{3})-?(\d{4})$')
        if not phone_format.match(phone_number) and phone_number != '':
            raise ValidationError('%s is not a valid phone number' % phone_number)
        return phone_number

    def clean_state(self):
        state = self.cleaned_data['state']
        state_format = re.compile('^[a-zA-Z]{2}')
        if not state_format.match(state) and state != '':
            raise ValidationError('%s is not a valid state' % state)
        return state

    def clean_zip(self):
        zip = self.cleaned_data['zip']
        zip_format = re.compile('^(\d){5}')
        if not zip_format.match(zip) and zip != '':
            raise ValidationError('%s is not a valid zip code' % zip)
        return zip
