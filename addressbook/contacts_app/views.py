from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render
from models import Contact, ContactForm
from django.http import HttpResponseRedirect


def index(request):
    latest_contact_list = Contact.objects.all()
    context = {"latest_contact_list": latest_contact_list}
    return render(request, "contacts_app_templates/index.html", context)


def edit_contact(request, contact_id=None):
    context = {}
    if request.method == 'POST':

        if contact_id:
            # editing contact
            contact_to_edit = Contact.objects.get(pk=contact_id)
            edit_form = ContactForm(request.POST, instance=contact_to_edit)

            if edit_form.is_valid():
                edit_form.save()
                return HttpResponseRedirect('/contacts/')
            else:
                context = {"form": edit_form, "contact": contact_to_edit}

        else:
            # create new contact
            new_form = ContactForm(request.POST)

            if new_form.is_valid():
                new_form.save()
                return HttpResponseRedirect('/contacts/')
            else:
                context = {"form": new_form}
    else:
        try:
            contact = Contact.objects.get(pk=contact_id)
            contact_form = ContactForm()
            context = {"form": contact_form, "contact": contact}
        except ObjectDoesNotExist:
            contact_form = ContactForm()
            context = {"form": contact_form}

    return render(request, "contacts_app_templates/contactDetail.html", context)


def delete_contact(request, contact_id):
    if request.method == 'POST':
        contact_to_delete = Contact.objects.get(id=contact_id)
        contact_to_delete.delete()
        return HttpResponseRedirect('/contacts/')


