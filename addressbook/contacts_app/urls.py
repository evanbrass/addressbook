from django.conf.urls import patterns, url
import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^(?P<contact_id>\d+)$', views.edit_contact, name='edit_contact'),
    url(r'^delete/(?P<contact_id>\d+)$', views.delete_contact, name='delete_contact'),
    url(r'^new/(?P<contact_id>\d+)$', views.edit_contact, name='edit_contact'),
    url(r'^new/$', views.edit_contact, name='edit_contact')
)