$(document).ready(function(){

    $('#myTab a').click(function (e) {
        e.preventDefault()
        $(this).tab('show')
    })

    $(".clickableRow").click(function() {
        window.document.location = $(this).attr("href");
    });

    $('#table_id').DataTable();

// THE END!
});